@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New Country
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <!-- New Country Form -->
                    <form action="{{ url('country')}}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}

                        <!-- Country Name -->
                        <div class="form-group">
                          <div class="row">
                            <label for="country-name" class="col-sm-3 control-label">Country name</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" id="country-name" class="form-control" value="{{ old('country') }}">
                            </div>
                          </div>
                          <div class="row">
                            <label for="country-code" class="col-sm-3 control-label">Country code</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" id="country-code" class="form-control" value="{{ old('country') }}">
                            </div>
                          </div>

                          <div class="row">
                            <label for="capital-city" class="col-sm-3 control-label">Capital city</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" id="capital-city" class="form-control" value="{{ old('country') }}">
                            </div>
                          </div>

                          <div class="row">
                            <label for="currency-code" class="col-sm-3 control-label">Currency code</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" id="currency-code" class="form-control" value="{{ old('country') }}">
                            </div>
                          </div>
                          <div class="row">
                            <label for="language" class="col-sm-3 control-label">Language</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" id="language" class="form-control" value="{{ old('country') }}">
                            </div>
                          </div>
                        </div>

                        <!-- Add Task Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-plus"></i>Add Country
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            @if (count($countries) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Countries
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped country-table">
                            <thead>
                                <th>Country</th>
                                <th>name</th>
                                <th>cioc</th>
                                <th>capital</th>
                                <th>region</th>
                                <th>flag</th>
                            </thead>
                            <tbody>
                                @foreach ($countries as $country)
                                    <tr>

                                        <td class="col-sm-2 table-text"><div>{{ $country->name }}</div></td>
                                        <td class="col-sm-2 table-text"><div>{{ $country->cioc }}</div></td>
                                        <td class="col-sm-2 table-text"><div>{{ $country->capital }}</div></td>
                                        <td class="col-sm-2 table-text"><div>{{ $country->region }}</div></td>
                                        <td class="col-sm-2 table-text"><img src="{{ $country->flag }}" style="max-width:100px;"></img></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
