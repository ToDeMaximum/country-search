@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
              <div class="panel-heading">
                  Search Country
              </div>

              <div class="panel-body">
                <form action="{{ url('/')}}" method="post" class="form-horizontal">
                    {{ csrf_field() }}

                    <!-- Country Name -->
                    <div class="form-group">
                      <div class="row">
                        <label for="country-name" class="col-sm-3 control-label">Country name</label>

                        <div class="col-sm-6">
                            <input type="text" name="name" id="country-name" class="form-control" value="{{ old('country') }}">
                        </div>
                      </div>
                      <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-6">
                              <button type="submit" class="btn btn-default">
                                  <i class="fa fa-btn fa-plus"></i>Search
                              </button>
                          </div>
                      </div>
                  </from>
              </div>
        </div>
        @if (count($countries) > 0)
            <div class="panel panel-default">
                <div class="panel-heading">
                    Countries
                </div>

                <div class="panel-body">
                    <table class="table table-striped country-table">
                        <thead>
                            <th>Country</th>
                            <th>name</th>
                            <th>cioc</th>
                            <th>capital</th>
                            <th>region</th>
                            <th>flag</th>
                        </thead>
                        <tbody>
                            @foreach ($countries as $country)
                                <tr>

                                    <td class="col-sm-2 table-text"><div>{{ $country->name }}</div></td>
                                    <td class="col-sm-2 table-text"><div>{{ $country->cioc }}</div></td>
                                    <td class="col-sm-2 table-text"><div>{{ $country->capital }}</div></td>
                                    <td class="col-sm-2 table-text"><div>{{ $country->region }}</div></td>
                                    <td class="col-sm-2 table-text"><img src="{{ $country->flag }}" style="max-width:100px;"></img></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    </div>
@endsection
