<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('cioc');
            $table->string('capital');
            $table->string('region');
            $table->string('flag');
            $table->timestamps();
        });

        Schema::create('currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id');
            $table->string('currency_code');
            $table->string('symbol');
            $table->timestamps();

        });

        Schema::create('timezones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id');
            $table->string('timezone');
            $table->timestamps();

        });

        Schema::create('calling_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id');
            $table->string('dialing_code');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('calling_codes');
      Schema::drop('timezones');
      Schema::drop('currencies');
      Schema::drop('countries');
    }

}
