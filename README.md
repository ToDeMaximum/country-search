# country-search

This is a simple application to 

1. search for a country (using a simple text box)
2. if it already exists in the database return from there
3. if it doesn't exist save it in the database

This was done in half a day as part of a coding challenge so there are a couple of nice to haves that were missing (see comments) which I haven't come back to fix due to origionally being some throwaway code.

The docker config was complex to set up, it uses laradock (see https://laradock.io/) just to make it viewable but may find it easier to just use your regular local dev environment

Currently looking into setting up docker-compose to make the setup easier (based on other projects).

#

to run and install everything you will need to do the following, these instructions are based on Ubuntu so you may need to adjust depending on your OS. 

1. clone the directory into projects folder
2. cd country-search/laradock
3. copy env-example to .env
4. cd ..
5. composer install
6. docker-compose up -d nginx mysql redis workspace
7. docker exec --ti laradock_workspace_1 bash
8. php artisan key:generate
9. php artisan migrate

After running the above you should have a fully working docker container with laravel installed.

primary route is the index and you will be able to search for new countries (where it will save to database if doesn't exist otherwise pull from restcountries.eu) 
