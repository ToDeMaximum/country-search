<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @Given there is an :arg1
     */
    public function thereIsAn($arg1)
    {
        throw new PendingException();
    }

    /**
     * @When i input the :arg1 on the task list
     */
    public function iInputTheOnTheTaskList($arg1)
    {
        throw new PendingException();
    }

    /**
     * @When click submit
     */
    public function clickSubmit()
    {
        throw new PendingException();
    }

    /**
     * @Then i should have :arg1 on the task list
     */
    public function iShouldHaveOnTheTaskList($arg1)
    {
        throw new PendingException();
    }

    /**
     * @Then have :arg1 item in the list
     */
    public function haveItemInTheList($arg1)
    {
        throw new PendingException();
    }
}
