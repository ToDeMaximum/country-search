Feature: Task list
  In order to add a task
  As a user
  I need to be able to add tasks to the list
  I need to be able to remove tasks from the list
  I need to be able to edit tasks on the list

  Scenario: Adding a task
    Given there is an "named task"
    When i input the "named task" on the task list
    And click submit
    Then i should have "named task" on the task list
    And have 1 item in the list
