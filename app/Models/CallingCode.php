<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CallingCode extends Model
{
  protected $table = 'calling_codes';

  protected $fillable = [
      'country_id',
      'dialling_code'
  ];

  public function country(){
    return $this->hasOne('App\Models\Country');
  }

}
