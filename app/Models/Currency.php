<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
  protected $table = 'currencies';

  protected $fillable = [
      'currency_code',
      'country_id',
      'symbol'
  ];

  public function country(){
    return $this->hasOne('App\Models\Country');
  }
}
