<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Timezone extends Model
{
  protected $table = 'timezones';

  protected $fillable = [
      'timezone',
      'country_id'
  ];

  public function country(){
    return $this->hasOne('App\Models\Country');
  }
}
