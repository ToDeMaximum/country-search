<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class country extends Model
{
    protected $table = 'countries';

    protected $fillable = [
          'name',
          'cioc',
          'capital',
          'region',
          'flag'
    ];

    // overwriting the create method so we can update all the relations, not the best method but ensures that we can fill all the correct data, if done again (had more time)
    // would use relational mapping correctly (rather than the loose migrations)..

    public static function create(array $attributes = Array() ) {

      $country = new Country($attributes);

      \DB::transaction(function() use ($country,$attributes) {

         $country->save();

         foreach($attributes['currencies'] as $currency) {
           $c = new Currency;
           $c->fill([
             'country_id' => $country->id,
             'currency_code' => $currency->code,
             'symbol' => $currency->symbol
           ]);
           $c->save();
         }

         foreach($attributes['callingCodes'] as $callCode) {
           $code = new CallingCode;
           $code->fill([
             'country_id' => $country->id,
             'calling_code' => $callCode,
           ]);
           $code->save();
         }

         foreach($attributes['timezones'] as $timezone) {
           $tz = new Timezone;
           $tz->fill([
             'country_id' => $country->id,
             'timezone' => $timezone,
           ]);
           $tz->save();
         }

      });
    }

    public function currency()
    {
        return $this->hasMany('App\Models\Currency');
    }
    public function callingCodes()
    {
        return $this->hasMany('App\Models\CallingCode');
    }
    public function timezones()
    {
        return $this->hasMany('App\Models\Timezone');
    }
}
