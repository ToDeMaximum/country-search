<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

use App\Http\Controllers\Country\CountryController as Country;
use Illuminate\Http\Request;

Route::group(['middleware' => ['web']], function () {
    /**
     * Add New Country
     */
    Route::post('/', function( Request $request) {
        return view('/country/search', [
            'countries' => Country::search($request->name)
        ]);
    });

    Route::get('/', function() {
        return view('/country/search', [
            'countries' => Country::get('asc','name')
        ]);
    });

    Route::get('/index', function() {
      return view('/country/index', [
        'countries' => Country::get('asc','name')
      ]);
    });
});
