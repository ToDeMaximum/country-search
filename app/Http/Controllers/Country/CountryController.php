<?php

namespace App\Http\Controllers\Country;

use App\Models\Country;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;


class CountryController extends Controller
{
	/*
		would normally start user input with a validator class, time constraint made this difficult however isn't much to implement
	*/

	public static function search($term)
	{
		$term = htmlspecialchars($term);
		$countries = Country::select(['name', 'cioc', 'capital', 'region', 'flag'])
			->where('name', 'LIKE', '%' . $term . '%')->get();

		if (empty($countries->toArray())) {
			$client = new Client([
				'base_uri' => 'https://restcountries.eu/rest/v2/name/',
				'timeout' => 5.0,
				'exceptions' => false
			]);

			$response = $client->get($term);
			if ($response->getStatusCode() == 404) {
				return Country::get()->all();
			}
			$response = json_decode($response->getBody()->getContents());
			$countries = Country::hydrate($response);

			foreach ($countries as $country) {
				$country = Country::create($country->toArray());
			}

		}
		return $countries;
	}

	public static function get(string $direction, ?string $order_by = null)
	{
		return Country::orderBy($direction, $order_by)->get();
	}

}
